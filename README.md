
# JS Webdriver.io task 1

This repo contains the project to execute the webdriver.io task 1, trying to follow POM. When initializing wdio on the project folder, for framework Mocha was selected.  
This project contains other aspects shown in training, like:
- Type: 'Module' set in package.json file
- Takes a screenshot in case the test fails.
- Code follows principles of layers in Test Automation Framework.

## Test Case steps

1. Open https://pastebin.com/ in any browser.
2. Create a new paste with the following attributes:
  - Code: 'Hello from WebDriver'
  - Paste Expiration: 10 Minutes
  - Paste Name/Title: 'helloweb'

**NOTE:**
In the automation script, there is no step performed to click the Create a New Paste button, since task 2 involves the creation of the Paste and verify that the selected data is shown in the new page.  

## Automation Logic  
The file ./src/tests/pastebin.test.js contains the test suite to execute.  
It consists of 4 individual tests, one of those(Verify page title) was included as some way to verify that the pastebin page loaded.  
Each of the specified attributes for the new paste are separated into individual tests. Each individual test, performs the specified action and verifies that the shown values correspond to what was selected / written.

## Installation

Clone this git repo

```bash
  git clone https://gitlab.com/ignaciops/js-webdriver-task1.git
  cd js-webdriver-task1
```
Install the npm packages

```bash
  npm install
```


## Running Tests
For this task, the package.json file was modified for the test script to execute the wdio run command.  
package.json file:
```javascript
  {
    "scripts": {
      "test": "wdio run ./src/configs/wdio.conf.js"
    },
  }
```

To run tests, run the following command

```bash
  npm run test
```

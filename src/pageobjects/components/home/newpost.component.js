export class NewPostComponent {
  
  get rootEl() {
    return $('.post-create');
  };

  get textArea() {
    return this.rootEl.$('textarea');
  };

  get expirationResults() {
    return this.rootEl.$('#select2-postform-expiration-results');
  }

  options(param) {
    const selectors = {
      category: '#select2-postform-category_id-container',
      tags: '.bootstrap-tagsinput input',
      syntaxHighlight: '#select2-postform-format-container',
      expiration: '#select2-postform-expiration-container',
      exposure: '#select2-postform-status-container',
      //folder: '',
      title: '#postform-name',
   };
   return this.rootEl.$(selectors[param.toLowerCase()]);
  };

  expiration(param) {
    const selectors = {
      'never': '//li[contains(@data-select2-id, "-N")]',
      'burn': '//li[contains(@data-select2-id, "-B")]',
      '10 min': '//li[contains(@data-select2-id, "-10M")]',
      '1 hour': '//li[contains(@data-select2-id, "-1H")]',
      '1 day': '//li[contains(@data-select2-id, "-1D")]',
      '1 week': '//li[contains(@data-select2-id, "-1W")]',
      '2 weeks': '//li[contains(@data-select2-id, "-2W")]',
    };
    return $(`${selectors[param.toLowerCase()]}`);
  }
};

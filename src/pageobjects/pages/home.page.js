import { NewPostComponent } from "../components/home/newpost.component.js";
export class HomePage {
  constructor () {
    this.newPostComponent = new NewPostComponent();
  }
  
  async open() {
    await browser.maximizeWindow();
    await browser.url('https://pastebin.com');
  }
}
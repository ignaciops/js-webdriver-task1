import { HomePage } from '../pageobjects/pages/home.page.js';

const homePage = new HomePage();

describe('Pastebin test suite for task 1', () => {
  // open the pastebin website
  before(async () => {
    homePage.open();
  })
  
  it('Verify Pastebin page title', async () => {
    const expectedTitle = 'Pastebin.com - #1 paste tool since 2002!';
    await expect(browser).toHaveTitle(expectedTitle);
  });

    // Write 'Hello from WebDriver' into text area
  it('Write Hello from Web Driver into text area', async () => {
    const textToWrite = 'Hello from WebDriver';
    await homePage.newPostComponent.textArea.setValue(textToWrite);
    await expect(homePage.newPostComponent.textArea).toHaveValue(textToWrite);
  });

  //   // Set Paste expiration to 10 minutes.
  // it('Set Paste expiration to 10 minutes', async () => {
  //   await homePage.newPostComponent.options('expiration').scrollIntoView();
  //   await homePage.newPostComponent.options('expiration').click();
  //   await homePage.newPostComponent.expiration('10 Min').click();
  //   await expect(homePage.newPostComponent.options('expiration')).toHaveText('10 Minutes');
  // });

    // Input "helloweb" in Paste Name text field
  it('Input helloweb in Paste Name/Title text field', async () => {
    await homePage.newPostComponent.options('title').setValue('helloweb');
    await expect(homePage.newPostComponent.options('title')).toHaveValue('helloweb');
  });

});